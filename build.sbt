name := "t-digest"

organization := "com.tdunning"

enablePlugins(GitVersioning)

git.useGitDescribe := true

scalaVersion := "2.12.2"
